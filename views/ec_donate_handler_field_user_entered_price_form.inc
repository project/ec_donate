<?php


/**
 * @file
 * Implements display handler for user entered price form.
 */

class ec_donate_handler_field_user_entered_price_form extends views_handler_field {
  function render($values) {
    $node = node_load($values->{$this->field_alias});

    // Add an 'user_price' form
    if (isset($node->ptype) && ec_product_attributes_get($node, 'user_price')) {
      return drupal_get_form('ec_checkout_product_form', array());
    }
  }
}
