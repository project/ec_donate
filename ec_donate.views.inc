<?php


/**
 * @file
 * Views integration for ec_donate module.
 */

/**
 * Implementation of hook_views_data().
 */
function ec_donate_views_data() {

  $data['ec_product']['product_is_donation'] = array(
    'title' => t('Mark product as a donation'),
    'help' => t('Allows distinction of donation products from non-donation products.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    ),
  );
  $data['ec_product']['user_price'] = array(
    'title' => t('Enable user-entered-price'),
    'help' => t('Allows user to enter price'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    ),
  );
  $data['ec_product']['hide_product_price'] = array(
    'title' => t('Enable hide-product-price'),
    'help' => t('Allows admin to hide the product price. Useful for donations.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    ),
  );
  $data['ec_product']['hide_cart_link'] = array(
    'title' => t('Enable hide_cart_link'),
    'help' => t('Allows admin to hide the "add to cart" link.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    ),
  );
  $data['ec_product']['use_product_cart_form'] = array(
    'title' => t('Enable use of product cart form'),
    'help' => t('Allows admin to display form with product for selecting quantity.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    ),
  );
  $data['ec_product']['use_custom_response'] = array(
    'title' => t('Enable use of custom response'),
    'help' => t('Provides "Thank You" text for donations on donation receipts.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    ),
  );
  $data['ec_product']['custom_response'] = array(
    'title' => t('Custom response for product'),
    'help' => t('"Thank You" text of product'),
    'field' => array(
      'handler' => 'views_handler_field_markup',
    ),
  );
  $data['ec_transaction']['custom_response'] = array(
    'title' => t('Custom response for completed donation'),
    'help' => t('"Thank You" text message used on completed transaction'),
    'field' => array(
      'handler' => 'views_handler_field_markup',
    ),
  );
  $data['ec_transaction']['donation_amount'] = array(
    'title' => t('Donation amount'),
    'help' => t('Amount of transaction considered as a donation'),
    'field' => array(
      'handler' => 'ec_common_views_handler_field_format_currency',
      'click sortable' => TRUE,
    ),
  );
  $data['ec_transaction_product']['product_is_donation'] = array(
    'title' => t('Donation'),
    'help' => t('Indicates that the product is a donation.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean'
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    ),
  );
  return $data;
}