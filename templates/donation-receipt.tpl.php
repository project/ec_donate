<div id="donation-receipt-<?php print $txnid; ?>" class="donation-receipt<?php echo $shippable ? ' shippable' : ' not-shippable'; ?>">
  <div class="header">
    <h1>Donation Receipt from <?php print variable_get('site_name', 'Drupal'); ?></h1>
  </div>
  <div id="address-payment">
    <div class="donation-receipt-addresses">
      <?php if ($shippable): ?>
        <div id="shipping-address" class="address"><?php print $shipping_address; ?></div>
      <?php endif; ?>
        <div id="billing-address" class="address"><?php print $billing_address; ?></div>
      </div>
      <div class="donation-payment clear-both clear-block">
        <div class="donation-payment-details">
          <div class="txn-datetime"><?php print $transaction_datetime; ?></div>
          <div class="paid-with"><?php print $paid_with; ?></div>
          <div class="txn-number"><?php print $transaction_number; ?></div>
          <div class="rcpt-number"><?php print $receipt_number; ?></div>
        </div>
      </div>
  </div>

    <div class="donation-receipt-details">
      <table>
        <tr>
          <th>Description</th>
          <th>Total</th>
        </tr>
      <?php foreach ($items as $item) {
      ?>
        <tr>
          <td><?php print $item->title; ?></td>
          <td class="item-price"><?php print $item->formatted_price; ?></td>
        </tr>
      <?php } ?>
      <?php if (!empty($misc)) {
      ?>
        <tr>
          <td colspan="1" class="subtotal-title">Subtotal</td>
          <td class="subtotal-amount"><?php print $subtotal; ?></td>
        </tr>
      <?php foreach ($misc as $item) {
      ?>
          <tr>
            <td colspan="1" class="subtotal-title"><?php echo $item->description; ?></td>
            <td class="subtotal-amount"><?php print $item->price; ?></td>
          </tr>
      <?php } ?>
      <?php } ?>
      <tr>
        <td colspan="1" class="total-title">Total</td>
        <td class="total-amount"><?php print $gross; ?></td>
      </tr>
    </table>
  </div>
  <div class="donation-custom-response"><?php print $custom_response; ?></div>
</div>
<div><?php print $additional; ?></div>


