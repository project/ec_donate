<?php

/**
 * @file
 * Preprocess function for all ec_store templates.
 */

/**
 * Implementation of hook_preprocess_hook().
 */
function template_preprocess_donation_receipt(&$variables) {
  drupal_add_css(drupal_get_path('module', 'ec_donate') .'/css/donation_receipt.css');

  $txn =& $variables['transaction'];
  
  $variables['template_files'][] = 'invoice-'. $txn->type;
  $variables['template_files'][] = 'invoice-'. $txn->payment_method;
  $variables['template_files'][] = 'invoice-'. $txn->type .'-'. $txn->payment_method;

  $variables['billing_address'] = theme('formatted_address', $txn->address['billing']);
  if (isset($txn->shippable) && $txn->shippable) {
    $variables['shipping_address'] = theme('formatted_address', $txn->address['shipping']);
  }
  
  $subtotal = 0;
  foreach ($txn->items as $nid => $item) {
    $txn->items[$nid]->price = ec_store_adjust_misc($txn, $item);
    $txn->items[$nid]->formatted_price = format_currency($txn->items[$nid]->price);
    $node = node_load($nid);
    $variables['thankyou_text'] =  content_format('field_donate_receipt_text', $node->field_donate_receipt_text[0], 'default', $node);
  }
  
  $variables['gross'] = format_currency(ec_store_transaction_calc_gross($txn));

  if (!empty($txn->additional)) {
    $variables['additional'] = drupal_render($txn->additional);
  }

  $variables+= (array)$variables['transaction'];

  // process receipt vars
  $receipt = & $variables['receipt'];
  $variables['paid_with'] = t('Paid with ') . $receipt->description;
  $variables['transaction_number'] = t('Transaction #: ') . $txn->txnid;
  $variables['receipt_number'] = t('Receipt #: ') . $receipt->erid;
  $variables['transaction_datetime'] = date(' F j, Y', $receipt->changed) . date(' g:iA', $receipt->changed);
  $variables['custom_response'] = t($txn->custom_response);
  $defaults = array(
    'additional' => '',
  );

  $variables+= $defaults;
}

/**
 * 
 */
function template_preprocess_donation_receipt_formatted_address(&$variables) {
  if (is_object($variables['address'])) {
    $variables['address'] = (array)$variables['address'];
  }
  $address =& $variables['address'];
  $variables['name'] = check_plain(!empty($address['fullname']) ? $address['fullname'] : $address['firstname'] .' '. $address['lastname']);
  
  $variables['street_address'] = '';
  if ($variables['include_name']) {
    $variables['street_address'] = $variables['name'] .'<br />';
  }
  $variables['street_address'] .= $address['street1'];
  if ($address['street1'] && $address['street2']) {
    $variables['street_address'] .= '<br />';
  }
  if ($address['street2']) {
    $variables['street_address'] .= $address['street2'];
  }
  
  $variables['country'] = ec_store_get_country($address['country']);
  
  $variables+= $address;
  
  $country = drupal_strtolower($variables['address']['country']);
  $variables['template_files'][] = 'formatted-address-'. $country;
}