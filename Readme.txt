eCommerce module: ec_donate

Description: provides for donations to occur through eCommerce

This module provides for the ability to create donation-type products where the user enters the amount of the donation.

All donations are extensions of the base eCommerce product allowing us to
use all the API features of ec_product. There are 3 possible methods that are
available for donations via this module:
1) A donation that doesn't use the shopping cart. This becomes a simple
payment form and doesn't have any cart functionality.
2) A donation that does use a cart. This is just like any other product
with full cart capability.
3) A donation that is incorporated as an additional item in the
checkout process. This provides for a donation on the payment page of the checkout process.

To create a donation product:

- Enable the ec_donate module in the e-Commerce Product Features section.

- Visit admin/ecsettings/donations
  -set your Completed Donation URL and Your Minimum Donation Amount. These settings will
  apply to all donation-products that you create on this installation/site but may
  be individually selected in a future release.

- Visit admin/ecsettings/products/types/donation/features
  - ensure that you have the following features listed for this product type
    - Product is donation
    - Donate now
    - Use custom response
    - Purchase via shopping cart

- Visit node/add/product/donation
 - Enter the details as needed.
 - Expand the Special Options section
  - Set "Disallow purchase via Shopping Cart" - determines whether the "Add to Cart" button is displayed on the product
  - Set "Use Donate Now Button" - determines whether the Donate Now button appears on the product- just like "Buy Now"
  - Set "Allow use on Store Checkout" - determines whether this product is displayed on the normal Store Checkout page so that
      customers can add to their purchase immediately
  - Set "Use Response Text" - this is displayed on the receipt for any donation





