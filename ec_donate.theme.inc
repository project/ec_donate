<?php


/**
 * @file
 * Provide theme functions
 */

function theme_ec_donate_donation_form($form) {
  // the default theme for this element looks at the $user
  // to determine its output. Here we unset that theme to set our own.
  $form['ec_customer_email']['#theme'] = 'ec_donate_mail_element';

  $output .= drupal_render($form);
  return '<div id="ec-donate-donation-form">' . $output . '</div>';
}

function theme_ec_donate_user_entered_price_form($form) {
  $output = '<div class="container-inline">';
  $output .= drupal_render($form);
  $output .= '</div>';
  return '<div id="ec-donate-user-entered-price-form">' . $output . '</div>';
}

function theme_ec_donate_oncheckout($form) {
  if ( count(element_children($form['donations'])) == 0  ) {
    return '';
  }
  $header = array(
    array('data' => '', 'class' => 'ec-donate-oncheckout-body'),
    array('data' => drupal_render($form['amount_text']), 'class' => 'ec-donate-oncheckout-amount'),
  );
  foreach (element_children($form['donations']) as $nid => $line) {
    $rows[] = array(
      'data' => array(
        array(
         'data' => drupal_render($form['donations'][$line]['body']),
         'class' => 'ec-donate-oncheckout-body',
        ),
        array(
         'data' => drupal_render($form['donations'][$line]['user_entered_price']),
         'class' => 'ec-donate-oncheckout-amount',
        ),
      ),
      'class' => 'ec-donate-oncheckout-row-item',
     );
  }
  $inner_content = theme('table', $header, $rows);
  $header = $rows = array();
  $rows[] = array(
    'data' => array(
       array(
         'data' => drupal_render($form['oncheckout_text']) . $inner_content,
         'class' => 'ec-donate-oncheckout-text',
         'colspan' => 2,
       ),
     ),
   );
  $content = theme('table', $header, $rows);
  return '<div class="ec-donate-oncheckout-items">' . theme('box', t('Donation'), $content) . '</div>';
}


function theme_ec_donate_mail_element($element) {
  global $user;

  $changeurl = NULL;
  if (!$user->uid) {
    #$changeurl = ' ' . l('(' . t('Login') . ')', 'user/login', array('query' => drupal_get_destination()));
  }

  return theme('box', t('Email address') . $changeurl, drupal_render($element));
}

function theme_ec_donate_amount_element($element) {
  return theme('box', t($element['#title']), '<h4>' . variable_get('payment_symbol', '') . $element['#value'] . '</h4>');
}

function theme_ec_donate_donation_review_form($form) {
  $header = array(
    array('data' => t('Donation'), 'id' => 'donation-header-label'),
    array('data' => t('Amount'), 'id' => 'amount-header-label'),
    t('')
  );
  $rows = array();
  if (!empty($form['items'])) {
    foreach ($form['items'] as $nid => $line) {
      if (is_numeric($nid)) {
        $rows[] = array(
          drupal_render($form['items'][$nid]['title']),
          array('data' => drupal_render($form['items'][$nid]['price']), 'align' => 'right'),
          drupal_render($form['items'][$nid]['options']),
        );
      }
    }
  }

  $rows[] = array('', '', '');
  foreach ($form['totals'] as $id => $line) {
    if (is_numeric($id)) {
      $rows[] = array(
        "<b>{$line['#title']}</b>",
        array('data' => isset($line['#value']) ? format_currency($line['#value']) : '', 'align' => 'right'),
        '',
      );
      drupal_render($form['totals'][$id]);
    }
  }
  $content = theme('table', $header, $rows);

  return theme('box', t('Donation Summary'), $content);
}

function theme_ec_donate_donation_receipt($txn, $type = 'billing', $break = 'text') {

  if (empty($txn->address[$type])) {
    return FALSE;
  }
  $data = drupal_clone((object) $txn->address[$type]);

  $donation = current($txn->items);

  // Cleanup values.
  if ($break != 'text') {
    foreach ($data as $key => $value) {
      if (is_scalar($value)) {
        $data->$key = check_plain($value);
      }
    }
  }
  $break = ($break == 'text') ? "\n" : '<br />';

  $address = '';
  if ((!empty($data->firstname) && !empty($data->lastname)) || !empty($data->fullname)) {
    if (!empty($data->firstname) || !empty($data->lastname)) {
      $address .= $data->firstname .' '. $data->lastname . $break;
    }
    else {
      $address .= !empty($data->fullname) ? $data->fullname . $break : '';
    }
    $address .= !empty($data->street2) ? $data->street1 . $break . $data->street2 . $break : $data->street1. $break;
    $address .= drupal_ucfirst($data->city) .", ". drupal_strtoupper($data->state) ." ". $data->zip . $break;
    $address .= ec_store_get_country($data->country) . $break;
    $address .= $data->phone . $break;
  }

  $output .= "Name: ". $address;

  $header = array('Donation', 'Amount');
  $rows = array(array($donation->title, $donation->price));

  $output .= theme('table', $header, $rows);
  
  $output .= "<p>";
  $output .= content_format('field_donate_receipt_text', $donation->field_donate_receipt_text[0]);
  $output .= "</p>";
  return $output;
}