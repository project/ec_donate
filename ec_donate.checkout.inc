<?php


/**
 * @file
 * Allow ec_donate_get_all('checkout') to hook into ec_donate
 */

function ec_donate_checkout_form(&$form, &$form_state) {
  if ($form_state['txn']->type != "ec_cart") {
    return;
  }

  // disable page caching
  global $conf;
 	$conf['cache']=CACHE_DISABLED; 
  
  $txn =& $form_state['txn'];
  // verify that this is allowed to happen
  if ( variable_get('ec_donate_oncheckout_enable', 0) == 0 ) {
    return;
  }

  // get all donations that can appear on checkout
  $nodes = _ec_get_oncheckout_donations($txn);
  if (count($nodes) == 0 ) {
    return;
  }
  $form['ec_donate'] = array();
  $form['ec_donate']['#theme'] = 'ec_donate_oncheckout';
  $form['ec_donate']['title'] = array(
    '#title' => t('Donation'),
  );
  $form['ec_donate']['amount_text'] = array(
    '#value' => t('Amount (in !currency)', array('!currency' => variable_get('ec_default_currency', 'your currency'))),
  );
  $form['ec_donate']['oncheckout_text'] = array(
    '#value' => variable_get('ec_donate_oncheckout_text', ''),
  );

  $form['ec_donate']['donations'] = array(
      '#tree' => TRUE,
  );
  foreach ($nodes as $node) {
    $form['ec_donate']['donations'][$node->nid] = array();
    $form['ec_donate']['donations'][$node->nid]['#theme'] = 'ec_donate_oncheckout_user_entered_price_element';
    $form['ec_donate']['donations'][$node->nid]['body'] = array(
      '#value' => $node->body,
    );
    $form['ec_donate']['donations'][$node->nid]['user_entered_price'] = array(
      '#type' => 'textfield',
      '#size' => 10,
    );
  }
}

function ec_donate_oncheckout_donation(&$form, &$form_state) {
}

function ec_donate_checkout_validate(&$form, &$form_state) {
  $txn = & $form_state['txn'];
    if ($form_state['values']['form_id'] == 'ec_checkout_form' && isset($form_state['values']['donations'])) {
    foreach ($form_state['values']['donations'] as $nid => $item) {
      // don't validate unless we have a value
      if (!empty($item['user_entered_price']) ) {
        $valid_amount = ec_donate_validate_donation_amount($item['user_entered_price']);
        if ($item['user_entered_price'] != $valid_amount ) {
          form_set_error('user_entered_price',
              t('Amount must be greater than or equal to @valid_amount', array('@valid_amount' => $valid_amount))
          );
        }
      }
    }
  }
}

function ec_donate_checkout_update(&$form, &$form_state) {
  $txn = & $form_state['txn'];
  if ($txn->type == "ec_cart" && isset($form_state['values']['donations'])) {
    foreach ( $form_state['values']['donations'] as $nid => $data ) {
      if (!empty($data['user_entered_price']) ) {
        // ensure that the cart has the item - just in case the user goes back to the cart
        ec_cart_update_item($nid, 1, $data);
      }
    }
    // ensure that the txn has the latest cart contents
    $txn->items = ec_cart_current();
    // ensure that the price for each donation is what the user entered
    foreach ( $txn->items as $item) {
      if ( isset($item->product_is_donation) && $item->product_is_donation ) {
        $item->price = $item->user_entered_price;
      }
    }
  }
}

/**
 * retrieve fully built nodes
 */
function _ec_get_oncheckout_donations(&$txn) {
  $nodes = array();
  $sql = 'SELECT node.nid
            FROM {ec_product}
            JOIN {node}
              ON node.nid = ec_product.nid
            WHERE display_on_checkout=1
              AND status=1
          ';
  $result = db_query($sql);
  while ($answer = db_fetch_object($result)) {
    // ensure that this donation is not already in the cart
    if ( !isset($txn->items[$answer->nid])) {
      $nodes[] = node_load($answer->nid);
    }
  }
  return $nodes;
}