<?php


/**
 * @file
 * Provide administration screens for ec_donate module.
 */

/**
 * Form for determining the policy for anonymous donations
 *
 * @ingroup form
 */
function ec_donate_settings() {
  $form = array();
  $form['ec_donate_return_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Completed Donation URL'),
    '#default_value' => variable_get('ec_donate_return_url', '%donation-receipt'),
    '#description' => t('Destination that user is sent to upon completion of the donation when not using a shopping cart. Use %donation-receipt to send the user to view the receipt.'),
  );
  $form['ec_donate_minimum_amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum Donation Amount'),
    '#default_value' => variable_get('ec_donate_minimum_amount', '1.00'),
    '#description' => t('Set the minimum amount for single donation.'),
  );
  $form['ec_donate_oncheckout_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable "on-checkout" Donations'),
    '#default_value' => variable_get('ec_donate_oncheckout_enable', ''),
    '#description' => t('Allow specified donations to appear on checkout page. Further settings are on the each donation node.'),
  );
  $form['ec_donate_oncheckout_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Text displayed on Store checkout page with on-checkout donations'),
    '#default_value' => variable_get('ec_donate_oncheckout_text', ''),
    '#description' => t('Text to be displayed to donor on receipt page and email'),
  );

  $description  = 'Select the text to be used as a response to the donation. This text will be visible to the donor ';
  $description .= 'on the email receipt sent to the donor and the web page that displays when the donation is completed. ';
  $description .= 'The <strong>default text</strong> ';
  $description .= 'is set here (below) and will apply to all products that ';
  $description .= '<ul>';
  $description .= '<li>have the feature <strong>custom response</strong> enabled<li>';
  $description .= '<li>have the setting <strong>Use custom response</strong> set to "Use default"<li>';
  $description .= '</ul>';
  $description .= 'The text you enter below will be used on products where you ';
  $description .= 'select <strong>Use default text</strong>. To set the custom text for a ';
  $description .= 'donation product, edit the product and insert your custom text there. ';

  $form['receipt_text_fs'] = array(
    '#type' => 'fieldset',
    '#title' => t('Response Text'),
    '#description' => t($description),
  );
  $form['receipt_text_fs']['ec_donate_receipt_text_default'] = array(
    '#type' => 'textarea',
    '#title' => t('Default response text'),
    '#default_value' => variable_get('ec_donate_receipt_text_default', ''),
    '#description' => t('Text to be displayed to donor on receipt page and email'),
  );

  return system_settings_form($form);
}