<?php


/**
 * @file
 * Provide integration with ecommerce features api
 */

/**
 * Implementation of hook_attributes().
 *
 *
 */
function ec_donate_feature_product_is_donation_attributes($node, $type = NULL, $a2 = NULL, $a3 = NULL, $a4 = NULL) {
  return array(
    'display_on_checkout' => ( $node->display_on_checkout ? TRUE : FALSE ),
    'donate_now' => ( $node->donate_now ? TRUE : FALSE ),
    'hide_buynow_link' => TRUE,
    'hide_product_price' => TRUE,
    'no_cart' => ( $node->no_cart ? TRUE : FALSE ),
    'no_quantity' => TRUE,
    'price' => $node->user_entered_price,
    'product_is_donation' => TRUE,
    'use_custom_response' => ( $node->use_custom_response ? TRUE : FALSE ),
    'use_product_cart_form' => TRUE,
    'user_price' => TRUE,
  );
}


/**
 * If we have this feature, then we force it to be set ON by returning TRUE
 * This overrides any data saved in the product table
 *
 * @param object $node
 * @param <type> $a4 - not used
 * @param <type> $a5 - not used
 * @param <type> $a6 - not used
 * @param <type> $a7 - not used
 *
 */
function ec_donate_feature_product_is_donation_load($node, $a4, $a5, $a6, $a7) {
  return array(
    'display_on_checkout' => ( $node->display_on_checkout ? TRUE : FALSE ),
    'donate_now' => ( $node->donate_now ? TRUE : FALSE ),
    'hide_product_price' => TRUE,
    'hide_buynow_link' => TRUE,
    'no_cart' => ( $node->no_cart ? TRUE : FALSE ),
    'price' => $node->user_entered_price,
    'product_is_donation' => TRUE,
    'use_custom_response' => ( $node->use_custom_response ? TRUE : FALSE ),
    'use_product_cart_form' => TRUE,
    'user_price' => TRUE,
  );
}

/*
 * called by ec_product_checkout_calculate
 * sets the price of the product to the price entered by the user
 *
 */
function ec_donate_feature_product_is_donation_product_calculate(&$txn, $item) {
  if (isset($item->user_price) && $item->user_price ) {
    $txn->items[$item->nid]->price = $item->user_entered_price;
  }
}

/**
 *
 * @param object $node - the donation product - $new_price, $old_price, $type, $txn
 * @param <type> $a4 - new price from hook_product_price_adjust()
 * @param string $a5 - old price - price from node before modification
 * @param string $type - type of transaction - one of: "ec_cart", "ec_donate"
 * @param <type> $txn - the transaction object
 */
function ec_donate_feature_product_is_donation_adjust_price(&$node, $new_price, $old_price, $type, $txn) {
  if (isset($node->user_price) && $node->user_price) {
    return $node->user_entered_price;
  }
}

/**
 * Implementation of hook_attributes().
 */
function ec_donate_feature_user_price_attributes($node, $a1 = NULL, $a2 = NULL, $a3 = NULL, $a4 = NULL) {
  return array(
    'user_price' => $node->user_price,
  );
}

function ec_donate_feature_product_is_donation_ec_checkout_validate_item(&$node, $type, &$qty, &$data, $severity, $valid_change) {
  if ($node->product_is_donation) {
    $node->price = floatval($data['user_entered_price']);
    return TRUE;
  }
  return FALSE;
}