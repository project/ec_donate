<?php


/**
 * @file
 * Provide rules integration with ec_donate
 */

/**
 * Implementation of hook_rules_event_info().
 */
function ec_donate_rules_event_info() {
  return array(
    'ec_donate_event_donation_form_submitted' => array(
      'label' => t('After donation form submitted'),
      'module' => 'ec Donate',
      'arguments' => array(
        'txn' => array(
            'type' => 'transaction',
            'label' => t('Transaction containing donation'),
        ),
      ),
    ),
    'ec_donate_event_recurring_donation_created' => array(
      'label' => t('After recurring donation created/submitted'),
      'module' => 'ec Donate',
      'arguments' => array(
        'txn' => array('type' => 'transaction', 'label' => t('Transaction containing recurring donation')),
      ),
    ),
  );
}


/**
* Implementation of hook_rules_condition_info().
* @ingroup rules
*/
function ec_donate_rules_condition_info() {
  return array(
    'ec_donate_condition_transaction_contains_donation' => array(
      'label' => t('Transaction contains donation'),
      'arguments' => array(
        'txn' => array('type' => 'transaction', 'label' => t('Transaction')),
      ),
      'module' => 'ec Donate',
    ),
    'ec_donate_condition_donation_is_recurring' => array(
      'label' => t('Donation is recurring'),
      'arguments' => array(
        'txn' => array('type' => 'transaction', 'label' => t('Donation')),
      ),
      'module' => 'ec Donate',
    ),
    'ec_donate_condition_credit_card_is_expired' => array(
      'label' => t('Credit Card is expired'),
      'arguments' => array(
        'txn' => array('type' => 'transaction', 'label' => t('Donation')),
      ),
      'module' => 'ec Donate',
    ),
  );
}

function ec_donate_condition_transaction_contains_donation($txn) {
  if ($txn->items) {
    foreach ($txn->items as $item) {
      if (isset($item->product_is_donation) && $item->product_is_donation) {
        return TRUE;
      }
    }
  }
  return FALSE;
}

function ec_donate_condition_donation_is_recurring($txn) {
  if ($txn->recurring) {
    return TRUE;
  }
}

