<?php


/**
 * @file
 * Provide default rules for rules integration
 */

function ec_donate_rules_defaults() {
  $config =
    array(
      'rules' =>
      array(
        'rules_999' =>
        array(
          '#type' => 'rule',
          '#set' => 'event_ec_donate_event_donation_form_submitted',
          '#label' => 'Email Donor after donation is made',
          '#active' => 1,
          '#weight' => '0',
          '#categories' =>
          array(
            0 => 'ec_donate',
          ),
          '#status' => 'custom',
          '#conditions' =>
          array(
          ),
          '#actions' =>
          array(
            0 =>
            array(
              '#weight' => 0,
              '#type' => 'action',
              '#settings' =>
              array(
                'to' => '[txn:txn-mail]',
                'from' => '[txn:site-mail]',
                'subject' => 'Receipt for Donation on [txn:txn-order-date]',
                'message' => 'Dear [txn:txn-billing-firstname] [txn:txn-billing-lastname],

Thank you for your donation of [txn:txn-gross] to our organization.
This is email is your receipt for your recent donation to this organization.

Date:[txn:txn-order-date] Transaction: [txn:txn-id]
Name: [txn:txn-billing-firstname] [txn:txn-billing-lastname]
[txn:txn-billing-address]
[txn:txn-bill-to]
Amount: [txn:txn-gross]

Thank you for your support!
[txn:site-name]
[txn:site-url]',
                '#eval input' =>
                array(
                  'token_rules_input_evaluator' =>
                  array(
                    'subject' =>
                    array(
                      0 => 'txn',
                    ),
                    'message' =>
                    array(
                      0 => 'txn',
                    ),
                    'from' =>
                    array(
                      0 => 'txn',
                    ),
                    'to' =>
                    array(
                      0 => 'txn',
                    ),
                  ),
                ),
              ),
              '#name' => 'rules_action_mail',
              '#info' =>
              array(
                'label' => 'Send a mail to an arbitrary mail address',
                'module' => 'System',
                'eval input' =>
                array(
                  0 => 'subject',
                  1 => 'message',
                  2 => 'from',
                  3 => 'to',
                ),
              ),
            ),
          ),
          '#version' => 6003,
        ),
      ),
  );
  return $config;
}